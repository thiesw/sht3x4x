from machine import I2C
import time, crc8


# Type of sensor used  (impl. note: value is also used as index into internal data structure; micropython does not support [Int]Enum)
SHT3x = const(0)
SHT4x = const(1)

# repeatability of sensor read  (impl. note: value is also used as index into internal data structure)
R_HIGH   = const(0)
R_MEDIUM = const(1)
R_LOW    = const(2)

# heater power (SHT4x only)
HEATER_LOW    = const(0)
HEATER_MEDIUM = const(1)
HEATER_HIGH   = const(2)



class SHT3x4x():
    """
    This class implements an interface to the SHT3x-DIS or SHT4x temperature and humidity
    sensor from Sensirion incl. CRC checking.
    Sensor type is auto-detected in constructor or may optionally be specified explicitly.
    Default I2C address is 0x44 but may be specified differently (0x44 - 0x46 allowed).
    Further details see data sheet of sensors, available from https://sensirion.com/products/downloads/
    For SHT3x clock stretching, periodic data acquisition mode, and alert mode are NOT implemented.

    Usage examples:
    
    from machine import Pin, I2C
    import sht3x4x as sht, time
    
    i2c = I2C(scl=Pin(5), sda=Pin(4), freq=400000)
    sensor = sht.SHT3x4x(i2c)
    #or:  sensor = sht.SHT3x4x(i2c, 0x45, sensorType = sht.SHT4x)

    temp, rh, crcOk = sensor.get_temp_rh()
    temp, rh, _ = sensor.get_temp_rh(sht.R_LOW)

    temp, rh, crcOk = sensor.get_temp_rh_with_heater(sht.HEATER_HIGH)
    
    # repeat read out on crc failure:
    for i in range(5):
        serial, crcOk = sensor.get_serial()
        if crcOk: break
        time.sleep_ms(10)
    if crcOk:
        print("Successfully read data:", serial)
        
    # kind of async. execution:
    sensor.send_read_temp_rh()
    #... perform some work here, e.g. send read command to another sensor ...
    temp, rh, crcOk = sensor.read_temp_rh()
    """

    # first index is sensor type (SHT3x, SHT4x), second index is the repeatability
    _CMD_READ = ( (b"\x24\x00", b"\x24\x0B", b"\x24\x16"),  # SHT3x no clock stretching
                  (b"\xFD", b"\xF6", b"\xE0") )             # SHT4x
    # maximum measurement duration (rounded up) for sensor type and repeatability
    _READ_WAIT_MS = ( (16, 7, 5),  (9, 5, 2) )
    # reset command for sensor type
    _CMD_RESET = ( b"\x30\xA2",  b"\x94")
    # only SHT4x: readout with heater, first index is short/long, second index is: low, medium, high heater power
    _CMD_HEATER_READ = ( (b"\x15", b"\x24", b"\x32"),  (b"\x1E", b"\x2F", b"\x39") )
    _HEATER_WAIT_MS = ( 130, 1030 )


    def __init__(self, i2c: I2C, addr: int = 0x44, *, fahrenheit: bool = False, sensorType: int = None):
        """
        Initialize a sensor object on the given I2C bus and accessed by the given address.
        Standard unit for temperature is celsius (°C) but my be changed to °F.
        sensorType may be None for auto detect (needs 3-45ms) or SHT3x|4x.
        May raise ValueError of no sensor is found.
        """
        if not isinstance(i2c, I2C):
            raise ValueError("Valid I2C object argument needed!")
        if addr < 0x44  or  addr > 0x46:
            raise ValueError("I2C address must be 0x44 - 0x46 (only possible values from Sensirion).")
        self._i2c = i2c
        self._addr = addr
        self._readFinishTick = None
        if sensorType == None:
            sensorType = self._detect_sensor()
        #print("SType: ", sensorType)
        if sensorType not in (SHT3x, SHT4x):
            raise ValueError("No valid sensor found on " + str(i2c))
        self._sensorType = sensorType
        # set conversion values depending on sensor and unit
        if sensorType == SHT3x:
            self._rhAdd = 0
            self._rhMult = 100 / 65535
        else:
            self._rhAdd = -6
            self._rhMult = 125 / 65535
        if fahrenheit:
            self._tempAdd = -49				# temperature conversion is the same for SHT3x and SHT4x
            self._tempMult = 315 / 65535
        else:
            self._tempAdd = -45
            self._tempMult = 175 / 65535


    def _detect_sensor(self) -> int:
        """
        Try to find SHT 3x or 4x sensor on object's I2C address. Needs 3-45ms.
        Return detected type (or None for no sensor found).
        """
        try:
            # read status from SHT3x
            for i in range(3):
                self._send(b"\xF3\x2D")
                time.sleep_ms(1)
                (raw, ok) = self._recv_words(1)
                #print("SHT3-Test: ", raw, ok)
                if ok:  return SHT3x		# successfully read valid data from SHT3x
                time.sleep_ms(10)
        except OSError as e:
            print("Exception on trying SHT3x:", e)
        try:
            # send "read serial" for SHT4x, try 3 times because of CRC
            for i in range(3):
                self._send(b"\x89")			# note: SHT3x returns status register for this, thus SHT4x after SHT3x
                time.sleep_ms(1)
                (raw, ok) = self._recv_words(2)
                #print("SHT4-Test: ", raw, ok)
                if ok:  return SHT4x		# successfully read valid data from SHT4x
                time.sleep_ms(10)
        except OSError as e:
            print("Exception on trying SHT4x:", e)
        return None


    def _send(self, bytes_: bytes) -> int:
        """Sends the given bytes over I2C to the sensor. Returns number of ACKs on I2C, should be len(bytes)."""
        return self._i2c.writeto(self._addr, bytes_)

    def _recv(self, num_bytes: int) -> bytes:
        """Read given number of bytes from the sensor using I2C."""
        return self._i2c.readfrom(self._addr, num_bytes)

    def _recv_words(self, num_words: int) -> Tuple[bytes, bool]:
        """
        Read given number of words (2 bytes each) with CRC-8 checksum validation.
        Return all bytes (incl. checksum bytes) and checksum ok.
        """
        raw = self._recv(num_words * 3)
        ok = True
        for idx in range(0, num_words*3, 3):
            ok = ok and (crc8.crc(raw[idx : idx+2]) == raw[idx+2])
        return (raw, ok)


    def send_read_temp_rh(self, repeatability: int = R_HIGH) -> int:
        """
        Send read command with given precision/repeatability (default is high) to sensor.
        Returns wait time in ms for the measurement (~ 2 - 16ms).
        After this time read_temp_rh() returns measurement values.
        """
        if repeatability not in (R_HIGH, R_MEDIUM, R_LOW):
            raise ValueError("Invalid repeatabillity value given: " + str(repeatability))
        self._send(self._CMD_READ[self._sensorType][repeatability])
        waitMs = self._READ_WAIT_MS[self._sensorType][repeatability]
        self._readFinishTick = time.ticks_add(time.ticks_ms(), waitMs)
        return waitMs

    def read_temp_rh(self, wait_for_read_finish: bool = True) -> Tuple[int, int, bool]:
        """
        Read the temperature and relative humidity incl. crc check.
        Waits for previous send_read() to finish if wait_for_read_finish is set.
        May raise "OSError: [Errno 19] ENODEV" if no previous read command was sent or not finished.
        Returns a tuple with temperature, rel. humidity (0..100) and checksum ok.
        """
        if wait_for_read_finish and self._readFinishTick:
            waitMs = time.ticks_diff(self._readFinishTick, time.ticks_ms())
            #print("read wait ms:", waitMs)
            if waitMs > 0:
                time.sleep_ms(waitMs)
        (raw, ok) = self._recv_words(2)
        self._readFinishTick = None
        temp = (raw[0] << 8) + raw[1]
        rh = (raw[3] << 8) + raw[4]
        temp = self._tempAdd + self._tempMult * temp
        rh = self._rhAdd + self._rhMult * rh
        return (temp, rh, ok)


    def get_temp_rh(self, repeatability: int = R_HIGH) -> Tuple[float, float, bool]:
        """
        Read the temperature and relative humidity incl. crc check with waiting for readout (~ 2-16ms).
        Temperature unit (°C/°F) is defined by constructor.
        Default precision/repeatability is high. Measurement duration depends on this.
        Returns a tuple with temperature, rel. humidity (0..100) and checksum ok.
        """
        time.sleep_ms(self.send_read_temp_rh(repeatability))
        return self.read_temp_rh(False)

    def get_temp_rh_with_heater(self, heater_power: int = 0, long: bool = True) -> Tuple[float, float, bool]:
        """
        Activate heater and perform high precision readout after a short wait.
        For SHT4x heater_power may be one of HEATER_* (0 for low 20mW, 1 for medium 110mW or 2 for high 200mW)
        and default is 1s duration which maybe reduced to 0.1s with long = False.
        For SHT3x with its low power heater (value of "heater_power" is ignored) duration is 5s or 0.5s.
        """
        longIdx = 1 if long else 0
        if self._sensorType == SHT3x:
            self.set_heater(True)
            time.sleep_ms( (5000 if long else 500) - self._READ_WAIT_MS[SHT3x][R_HIGH])
            result = self.get_temp_rh(R_HIGH)
            self.set_heater(False)
            return result
        else:
            if not isinstance(heater_power, int)  or  heater_power < 0  or  heater_power >= len(self._CMD_HEATER_READ[0]):
                raise ValueError("Invalid heater_power value given: " + str(heater_power))
            self._send(self._CMD_HEATER_READ[longIdx][heater_power])
            time.sleep_ms(self._HEATER_WAIT_MS[longIdx])
            return self.read_temp_rh(False)


    def soft_reset(self) -> None:
        """Send soft reset to sensor. Note: Execution time is 1.5 ms for SHT3x or 1 ms for SHT4x."""
        self._send(self._CMD_RESET(self._sensorType))
        

### SHT3x only:

    def set_heater(self, activate: bool) -> None:
        """Turn internal heater on or off, only for SHT3x. See also get_temp_rh_with_heater()."""
        if self._sensorType == SHT3x:
            self._send(b"\x30\x6D" if activate else b"\x30\x66")

    def get_status(self) -> Tuple[int, bool]:
        """
        Get status register of SHT3x sensor and checksum ok (for SHT4x returns 0,False).
        For details see data sheet, Table 18.
        """
        if self._sensorType == SHT3x:
            self._send(b"\xF3\x2D")
            time.sleep_ms(1)
            (raw, ok) = self._recv_words(1)
            return ( (raw[0] << 8) + raw[1], ok)
        else:
            return (0, False)

    def clear_status(self) -> None:
        """Clear status register (SHT3x only)."""
        if self._sensorType == SHT3x:
            self._send(b"\x30\x41")


### SHT4x only:

    def get_serial(self) -> Tuple[int, bool]:
        """Get serial number of SHT4x sensor and checksum ok (for SHT3x returns 0,False)."""
        if self._sensorType == SHT4x:
            self._send(b"\x89")
            time.sleep_ms(1)
            (raw, ok) = self._recv_words(2)
            return ( (raw[0] << 24) + (raw[1] << 16) + (raw[3] << 8) + raw[4], ok)
        else:
            return (0, False)

#end SHT3x4x
        


# Test:
if __debug__  and  __name__ == "__main__":
    from machine import Pin, I2C
    print("*** Start Debug-Execution ***")
    i2c = I2C(scl=Pin(5), sda=Pin(4), freq = 400000)		# 5+4 = D1 (SCL) + D2 (SDA)
    #i2c = I2C(scl=Pin(13), sda=Pin(12), freq = 400000)		# 13+12 = D7 (SCL) + D6 (SDA)
    print("Scanning", i2c, "...")
    addrs = i2c.scan()
    print("  -> ", addrs)
    for addr in addrs:
        try:
            print("-- Trying", addr, "=", hex(addr), "...")
            sensor = SHT3x4x(i2c, addr)
            print("Addr, Type (internal), serial, status: ", sensor._addr, sensor._sensorType, sensor.get_serial(), sensor.get_status())
            print("normal:     ", sensor.get_temp_rh())
            print("with heater:", sensor.get_temp_rh_with_heater())
            sensor.send_read_temp_rh()
            print("'async':    ", sensor.read_temp_rh())
        except Exception as e:
            print("=> Exception:", addr, e)
    print("*** END")
