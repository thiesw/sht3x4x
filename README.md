# SHT3x4x

Simple MicroPython compatible driver for Sensirion SHT3x and SHT4x temperature + humidity sensors.

One driver for both sensor types, auto-detection on init. or explicit specification possible.

Usage examples (see class description of SHT3x4x for more):
```
from machine import Pin, I2C
import sht3x4x as sht, time

sensor = sht.SHT3x4x(I2C(scl=Pin(5), sda=Pin(4), freq=400000))
sensor = sht.SHT3x4x(I2C(scl=Pin(5), sda=Pin(4), freq=400000), 0x45, sensorType = sht.SHT4x)

(temp, rh, crcOk) = sensor.get_temp_rh()
(temp, rh, _) = sensor.get_temp_rh(sht.R_LOW)

(temp, rh, crcOk) = sensor.get_temp_rh_with_heater(2)

for i in range(5):
	(serial, crcOk) = sensor.get_serial()
	if crcOk: break
	time.sleep_ms(10)
if crcOk:
	print("Successfully read data:", serial)
```

## License

Free use for anybody, but completely on your own risk.
